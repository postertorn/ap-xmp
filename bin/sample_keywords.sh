#!/bin/sh 

# get the keywords tag from a parent file using exiftool
# strip off the initial "Keywords      : " part
keywords=`exiftool -Keywords data/0030_000076+MB365UV_001_F.tif | sed 's/^Keywords[ :]*//'`
# $keywords will be a string like:
#
#     Resolution (PPI): 849, Position: 76, , , , , , , , , ,

# split into fields and grab the two keywords we want
resolution=`echo $keywords | awk -F ', ' '{ print $1 }'`
position=`echo $keywords | awk -F ', ' '{ print $2 }'`

# create the output dir if needed
if [ ! -e data/output ]; then
  mkdir data/output
fi
# delete the output file if it exists
output_file=data/output/some_image.tif
if [ -f $output_file ]; then
  rm $output_file
fi

# set the keywords in the destination file
exiftool -v -o data/output \
  -Keywords="$resolution" \
  -Keywords="$position" \
  data/some_image.tif

