#!/bin/sh

# This script shows how to copy data from a parent image files in JSON format
# and pass that data to a child image.
# 
# The JSON created by this script has the following structure.
# 
#    [{
#      "SourceFile": "data/some_image.tif",
#      "Source": "0030_000076",
#      "ObjectName": "Georgian NF 61, 38v",
#      "Keywords": ["Resolution (PPI): 849","Position: 76"," "," "," "," "," "," "," "," "," "," "]
#    }]
#
# Note the SourceFile element. When the JSON file is created this value will be
# set to the parent file name. In order to read in JSON using exiftool, the
# SourceFile must be (somewhat counterintuitively) set to the name of
# destination file. This replacement is handled below when the JSON file is
# created.
# 
# Apart from the SourceFile value the JSON file contains only the ObjectName,
# Source, and Keywords tags and can be used to copy that content to the
# destination file using the exiftool -j=<FILE> option.

# output dir
output_dir=data/output
# the parent file
parent=data/0030_000076+MB365UV_001_F.tif
# JSON file to hold the data
json_file=$output_dir/`basename $parent`.json
# the destination file
dest=data/some_image.tif

# create the output dir if needed
if [ ! -e $output_dir ]; then
  mkdir $output_dir
fi
# delete the output file if it exists
output_file=$output_dir/some_image.tif
if [ -f $output_file ]; then
  rm $output_file
fi

# copy the specified tags to the JSON file
# note that we have to change the SourceFile from the parent file name to the
# destination file name
exiftool  $parent -j -Source -ObjectName -Keywords | sed "s!$parent!$dest!" > $json_file

# copy the JSON file data to the destination file; and write new file to output
# dir
exiftool -v -o $output_dir -json=$json_file $dest

# # clean up
if [ -f $json_file ]; then
  rm $json_file
fi
