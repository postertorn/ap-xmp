#!/bin/sh

output_dir=data/output
# parent=data/0030_000076+MB365UV_001_F.tif
dest=data/some_image.tif

# ONE-WORD TEXT VALUES
# 
# One-word values (i.e., values without spaces) can be handled simply. 
# When concatenating arguments, do not put quotation marks around simple values
# GOOD: args="-xmp-ap:MyTag=value"
#  BAD: args="-xmp-ap:MyTag=\"value\""

args=

# BAGS
#
# ID_Parent_File is an XMP 'bag', an unordered list; bag entries can be added
# one at time by setting the same XMP tag multiple times

# ap:ID_Parent_File
args="$args -xmp-ap:ID_Parent_File=file_a.tif"
args="$args -xmp-ap:ID_Parent_File=file_b.tif"
args="$args -xmp-ap:ID_Parent_File=file_c.tif"
args="$args -xmp-ap:ID_Parent_File=file_d.tif"
args="$args -xmp-ap:ID_Parent_File=file_e.tif"
args="$args -xmp-ap:ID_Parent_File=file_f.tif"
args="$args -xmp-ap:ID_Parent_File=file_g.tif"

# INTEGERS
#
# Integers work just like one-word text values.

# ap:DAT_Bits_Per_Sample
args="$args -xmp-ap:DAT_Bits_Per_Sample=8"
# ap:DAT_Samples_Per_Pixel
args="$args -xmp-ap:DAT_Samples_Per_Pixel=3"

# BOOLEANS
#
# Booleans: the following two values are booleans; booleans will be
# True or False; exiftool understands Yes and No as well.
# ap:DAT_Joining_Same_Parts_of_Folio
args="$args -xmp-ap:DAT_Joining_Same_Parts_of_Folio=yes"
# ap:DAT_Joining_Different_Parts_Of_Folio
args="$args -xmp-ap:DAT_Joining_Different_Parts_Of_Folio=no"


# ap:DAT_Type_of_Contrast_Adjustment
args="$args -xmp-ap:DAT_Type_of_Contrast_Adjustment=none"
# ap:DAT_Type_of_Image_Processing
args="$args -xmp-ap:DAT_Type_of_Image_Processing=enhancement"
# ap:DAT_Processing_Comments
args="$args -xmp-ap:DAT_Processing_Comments=None"
# ap:DAT_File_Processing_Rotation
args="$args -xmp-ap:DAT_File_Processing_Rotation=90"

# VALUES WITH SPACES AND MULTIPLE LINES
#
# values with spaces in them do not behave well as nicely as one-word values
# and cannot be concatenated. They have to be handled individually.

# ap:DAT_File_Processing
processing=" 
Grayscale image that is the result of the multiplication of the thresholded
grayscale image and the corresponding pca###r image (note: the ### indicates
the principal component bands used)
"
# ap:DAT_Software_Version
software_version="See DAT_Processing_Program"
# ap:DAT_Processing_Program
processing_program1="MATLAB 7.10.0.499 (2010a)"
processing_program2="PhotoShop CS3"

# CARRYOVER PARENT IMAGE DATE
# 
# Please also include the Source, ObjectName and Keywords from the Parent
source=0030_000076
object_name="Georgian NF 61, 38v"
keywords="Resolution (PPI): 849, Position: 76,  ,  ,  ,  ,  ,  ,  ,  ,  , "
resolution=`echo $keywords | awk -F ', ' '{ print $1 }'`
position=`echo $keywords | awk -F ', ' '{ print $2 }'`
# Note that these values are not set below, because the test TIFF file
# seemed to be malformaed and would not accept these values.
#
# The correct arguments would be
#
#   -Source="$source" \
#   -ObjectName="$object_name" \
#   -Keywords="$keywords"


# HOUSEKEEPING FOR SANPLE SCRIPTS
# create the output dir if needed
if [ ! -e $output_dir ]; then
  mkdir $output_dir
fi
# delete the output file if it exists
output_file=$output_dir/some_image.tif
if [ -f $output_file ]; then
  rm $output_file
fi

# THE COMMAND
# The basic exiftool command has this format:
#
#    exiftool -o <OUTPUT_DIRECTORY> <ARGS> <INPUT_FILE> 
#
# If you don't specify an output directory; exiftool will write out the updated
# file as input_file.tif in the original directory and put a backup copy in
# that same directory named input_file.tif_original. There are command-line
# switches that will overwrite the original:
#
#        -overwrite_original              Overwrite original by renaming tmp file
#        -overwrite_original_in_place     Overwrite original by copying tmp file
#
# For complete help on the command-line tool, enter exiftool without any
# arguments.

exiftool -v -o $output_dir $args \
  -xmp-dc:creator="Roger Easton"  \
  -xmp-dc:contributor="David Kelbe"  \
  -xmp-ap:DAT_File_Processing="$processing" \
  -xmp-ap:DAT_Software_Version="$software_version" \
  -xmp-ap:DAT_Processing_Program="$processing_program1" \
  -xmp-ap:DAT_Processing_Program="$processing_program2" \
  -Keywords="$resolution" \
  -Keywords="$position" \
  $dest 

