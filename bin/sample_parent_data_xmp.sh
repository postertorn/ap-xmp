#!/bin/sh

# This script shows how to copy data from parent image files in XMP format and
# pass that data to a child image.
# 
# Exiftool XMP does not support the IPTC Object name tag. This is because
# ObjectName is not included in ITPCCore the new XMP-based standard for
# ITPC (see here:
# http://www.iptc.org/site/Photo_Metadata/IPTC_Core_&_Extension/).  To ensure
# copying of the ObjectName to XMP files, I've created an adhoc XMP namespace
# that now supports one field: ObjectName.  It is defined thus:
#
#    %Image::ExifTool::UserDefined::adhoc = (
#      GROUPS        => { 0 => 'XMP', 1 => 'XMP-adhoc', 2 => 'Image' },
#      NAMESPACE     => { 'adhoc' => 'ns:adhoc' },
#      WRITABLE      => 'string',
#      ObjectName => { },
#    );
#
# The XMP created by this script has the following structure.
# 
#    <?xpacket begin='﻿' id='W5M0MpCehiHzreSzNTczkc9d'?>
#    <x:xmpmeta xmlns:x='adobe:ns:meta/' x:xmptk='Image::ExifTool 9.01'>
#    <rdf:RDF xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#'>
#    
#     <rdf:Description rdf:about=''
#      xmlns:adhoc='ns:adhoc'>
#      <adhoc:ObjectName>Georgian NF 61, 38v</adhoc:ObjectName>
#     </rdf:Description>
#    
#     <rdf:Description rdf:about=''
#      xmlns:pdf='http://ns.adobe.com/pdf/1.3/'>
#      <pdf:Keywords>Resolution (PPI): 849, Position: 76,  ,  ,  ,  ,  ,  ,  ,  ,  ,  </pdf:Keywords>
#     </rdf:Description>
#    
#     <rdf:Description rdf:about=''
#      xmlns:photoshop='http://ns.adobe.com/photoshop/1.0/'>
#      <photoshop:Source>0030_000076</photoshop:Source>
#     </rdf:Description>
#    </rdf:RDF>
#    </x:xmpmeta>
#    <?xpacket end='w'?>
#
# This file contains only the ObjectName, Source, and Keywords tags and can be
# used to copy that content to the destination file using the exiftool
# -tagsFromFile option.

# output dir
output_dir=data/output
# the parent file
parent=data/0030_000076+MB365UV_001_F.tif
# xmp file to hold the data
xmp_file=$output_dir/`basename $parent`.xmp
# the destination file
dest=data/some_image.tif

# HOUSEKEEPING FOR SANPLE SCRIPTS
# create the output dir if needed
if [ ! -e $output_dir ]; then
  mkdir $output_dir
fi
# delete the output file if it exists
output_file=$output_dir/some_image.tif
if [ -f $output_file ]; then
  rm $output_file
fi

# copy the specified tags to the XMP file
exiftool -tagsFromFile $parent -Source -ObjectName -Keywords $xmp_file

# copy the XMP file data to the destination file
exiftool -o $output_dir -tagsFromFile $xmp_file $dest
# a more explicit version of the command would be
#
#     exiftool -o $output_dir -tagsFromFile $xmp_file -Source -ObjectName -Keywords $dest
# 
# note that running the above command will generate a warning:
#
#    Warning: [minor] IPTC:Keywords exceeds length limit (truncated) - data/0030_000076+MB365UV_001_F.tif.xmp

# clean up
if [ -f $xmp_file ]; then
  rm $xmp_file
fi
