#!/bin/sh

# This script shows how to extract the Source, ObjectName, and Keywords tags
# from a parent image file for insertion into a child processed image. 
# 
# It reads the desired tags from the parent file and writes them to a temporary
# file for extraction. Each value is extracted and written to shell variable
# to be written to the destination file at the end of the script.

# FILE VARIABLES
dest_file=data/some_image.tif
parent_file=data/0030_000076+MB365UV_001_F.tif
# this tmp file is where we'll write the parent tags
tmp_file=/tmp/`basename $parent_file`.txt$$

# Write the tags we want to the temp file. 
exiftool -Source -ObjectName -Keywords $parent_file > $tmp_file

# The contents of the temp file will look something like this:
#
# Source                          : 0030_000076
# Object Name                     : Georgian NF 61, 38v
# Keywords                        : Resolution (PPI): 849, Position: 76,  ,  ,  ,  ,  ,  ,  ,  ,  ,
# 
# First, get the keywords.
keywords=`grep "^Keywords" $tmp_file | sed 's/^Keywords[ :]*//'`
# $keywords will be a string like:
#
#     Resolution (PPI): 849, Position: 76, , , , , , , , , ,

# split into fields and grab the two keywords we want
resolution=`echo $keywords | awk -F ', ' '{ print $1 }'`
position=`echo $keywords | awk -F ', ' '{ print $2 }'`

# get the source
source=`grep "^Source" $tmp_file | sed 's/^Source[ :]*//'`
# get the object name; NOTE that exiftool adds a space to the ObjectName tag
# (thus: "Object Name") for output display
object_name=`grep "^Object Name" $tmp_file | sed 's/^Object Name[ :]*//'`

# create the output dir if needed
if [ ! -e data/output ]; then
  mkdir data/output
fi
# delete the output file if it exists
output_file=data/output/some_image.tif
if [ -f $output_file ]; then
  rm $output_file
fi

# these argruments can be added to the argument list in the xmp_sample.sh
# script
# NOTE that the variables are surrounded by double-quotes
exiftool  -v -o data/output \
  -Source="$source" \
  -ObjectName="$object_name" \
  -Keywords="$resolution" \
  -Keywords="$position" $dest_file

# remove the tmp file
rm $tmp_file

