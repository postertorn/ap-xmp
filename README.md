# Project for Archimedes Palimpsest XMP metadata

Has an Archimedes Palimpsest (AP) XMP RNG file, configuration file for EXIF
tool, and sample scripts for inserting Archie XMP metadata.

# Configuration

The `conf` directory contains an `ExifTool_config` file. This file is needed
to run any of the sample scripts that work with XMP metadata. It should be copied 
to your home directory and renamed `.ExifTool_config` with a leading period. 

From the ExifTool website
<http://www.sno.phy.queensu.ca/~phil/exiftool/faq.html#Q11>:

      Note: The config file must be renamed at the command line because neither
      the Windows nor Mac GUI allow a file name to begin with a ".". To do this
      in Windows, run "cmd.exe" and type the following (pressing RETURN at the
      end of each line):

        cd %HOMEPATH%
        rename ExifTool_config .ExifTool_config

      or on a Mac, open the "Terminal" application (from the
      /Applications/Utilities folder) and type this command then press RETURN:

        mv ExifTool_config .ExifTool_config

Note: Upon opening the "Terminal" application, you should automatically be "in"
your home directory.

# Scripts

The `bin` directory contains a number of sample Bash shell scripts including:

* `xmp_sample.sh` a sample script for using exiftool to insert XMP metadata
  using commandline tag arguments

* `sample_keywords.sh`  a sample script that shows how to extract IPTC keywords
  from a parent image and set them in a child

* `sample_parent_data.sh`, `sample_parent_data_json.sh`,
  `sample_parent_data_xmp.sh` scripts that show how to read specific tags from
  a parent image to a temporary file for later copying into a child image; they
  demonstrate methods using a temporary text file, JSON file, and XMP file,
  respectively.

# Data

The `data` directory contains sample image files that can be used to run the
test scripts as well as JSON file that shows how to format the AP XMP metadata 
as JSON for copying to an image file.

# Extra

Supporting XML schema and field list for Archimedes Palimpsest metadata.
